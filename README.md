Supporting Information for "Deep-sea hydrothermal vent metagenome-assembled genomes provide insight into the phylum Nanoarchaeota" 

Emily St. John, Gilberto E. Flores, Jennifer Meneghin, Anna-Louise Reysenbach


File descriptions:

Metagenome-assembled deep-sea nanoarchaeotal genomes (MAGs) from Guaymas Basin (Gua-46), the Eastern Lau Spreading Center (M10-121; Genbank RZJO00000000) and the Mid-Cayman Rise (MC-1).